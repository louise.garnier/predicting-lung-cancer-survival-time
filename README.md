# Predicting lung cancer survival time

Challenge Data ENS https://challengedata.ens.fr/participants/challenges/33/ pour le cours Algortihmes en science des données 

## Equipe 

Garnier Louise

Liu Qihao

Teyssier Pierre

## Données 

Les données fournies pour le challenge se trouvent dans le dossier **challenge_files**.

## Code 

Le code contenant les implémentation des différentes méthodes essayées se trouve dans le dossier **notebooks**. Cinq méthodes ont été implémentées :
* Une régression de Cox classique en sélectionnant à la main les features pertinentes: **cox_regression.ipynb**
* Une régression de Cox avec extraction de features par PCA: **PCA_Cox_Regression.ipynb**
* Une méthode de Deep Learning avec l'algorithme Deepsurv: **deepsurv.ipynb**
* Une méthode de random survival forest: **random_survival_forest.ipynb**
* Une méthode mélangeant Deepsurv et CNN pour exploiter les données imagées: **CNN.ipynb**

Chaque notebook peut être lancé sans mofification et produit un fichier csv de résultat. Le score obtenu par chaque méthode est indiqué à la fin du notebook.

Le script Python **utils.py** contient des fonctions réutilisées dans le notebooks. 

De plus, le notebook **data_exploration** contient des explications et visualisation des données fournies. 

## Résultats

Le dossier **results** contient les fichier CSV produits par les notebooks qui ont servi au calcul du score de chaque méthode.

