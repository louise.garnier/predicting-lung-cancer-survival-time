import pandas as pd
import numpy as np
import os
from PIL import Image
import torchvision.transforms as transforms
import torch

DATA_PATH = '../challenge_files/'


def load_train_dataset():
    """
    Loads the training data (features and not images) contained in challenge_files/x_train/features and challenge_files/y_train
    Returns 2 dataframes (x and y)
    """
    clinical_data = pd.read_csv(DATA_PATH + 'x_train/features/clinical_data.csv', index_col=0)
    radiomics = pd.read_csv(DATA_PATH + 'x_train/features/radiomics.csv', index_col=0, header=1)
    x = pd.concat([clinical_data, radiomics], axis=1)
    y = pd.read_csv(DATA_PATH + 'y_train/output_VSVxRFU.csv', index_col=0)
    return x, y


def load_test_dataset():
    """
    Loads the testing data (features and not images) contained in challenge_files/x_test/features
    Returns a dataframe
    """
    clinical_data_test = pd.read_csv(DATA_PATH + 'x_test/features/clinical_data.csv', index_col=0)
    radiomics_test = pd.read_csv(DATA_PATH + 'x_test/features/radiomics.csv', index_col=0, header=1)
    return pd.concat([clinical_data_test, radiomics_test], axis=1)


def fillna(df):
    """
    Fills in NaN value in a dataframe df with the mean of the feature
    """
    for c in df.columns:
        if not isinstance(df[c].iloc[0], str):
            df[c] = df[c].fillna((df[c].mean()))


def extract_slice(scan, mask):
    """
    The scans and mask provided are 3D and we would like to use 2D images for resnet, so we extract the 2d slice
    that has the most tumor surface
    Returns the concatenation of the best slice of the mask and the corresponding slice of the scan
    """
    best_i_slice = 0
    max_tumor = 0
    for i_slice in range(92):
        tumor_surface = np.sum(mask[:, :, i_slice])
        if tumor_surface > max_tumor:
            best_i_slice = i_slice
            max_tumor = tumor_surface
    return np.concatenate((scan[:, :, best_i_slice], mask[:, :, best_i_slice]), axis=1)


def create_image_inputs(path):
    """
    Returns a list of resnet compatible tensors (one for each patient) and the index
    """
    index = []
    inputs = []
    for filename in os.listdir(path):
        ind = filename.split('_')[1].split('.')[0]
        index.append(ind)
        archive = np.load(f'{path}/{filename}')
        scan = archive['scan']
        mask = archive['mask']
        inputs.append(extract_slice(scan, mask))

    input_tensors = []
    resize = transforms.Resize(224)
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])  # Normalization required by resnet18
    for elt in inputs:
        PIL_img = Image.fromarray((255. * elt).astype('uint8'), 'L')
        resized_img = np.asarray(resize(PIL_img))
        # Resnet uses rgb images so we repeat our grayscale image 3 times
        rgb_img = np.repeat(resized_img[:, :, np.newaxis], 3, -1)
        tensor = torch.from_numpy(rgb_img)
        tensor = tensor.type('torch.FloatTensor')
        tensor = tensor.view(1, 3, 448, 224)
        input_tensors.append(normalize(tensor))

    return index, input_tensors
